use std::hash::{Hash, Hasher};

#[derive(Clone, Debug)]
pub enum Device {
    Thermometer {
        name: String,
        description: String,
    },
    Socket {
        name: String,
        description: String,
        is_power_on: bool,
    },
}

impl Device {
    pub fn get_display_str(&self) -> String {
        let (device_type, name, description) = match self {
            Device::Thermometer { name, description } => ("Thermometer", name, description),
            Device::Socket {
                name, description, ..
            } => ("Socket", name, description),
        };

        format!("{} \"{}\" ({})", device_type, name, description)
    }

    pub fn read_sensor(&self) -> String {
        match self {
            Device::Thermometer { .. } => "22 C".to_string(),
            Device::Socket { is_power_on, .. } => {
                if *is_power_on {
                    "200 Watt".to_string()
                } else {
                    "0 Watt".to_string()
                }
            }
        }
    }
}

impl PartialEq for Device {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Device::Thermometer { .. }, Device::Thermometer { .. }) => true,
            (Device::Socket { .. }, Device::Socket { .. }) => true,
            (
                Device::Thermometer {
                    name: self_name,
                    description: self_description,
                },
                Device::Socket {
                    name: other_name,
                    description: other_description,
                    ..
                },
            ) => self_name == other_name || self_description == other_description,
            (
                Device::Socket {
                    name: self_name,
                    description: self_description,
                    ..
                },
                Device::Thermometer {
                    name: other_name,
                    description: other_description,
                },
            ) => self_name == other_name || self_description == other_description,
        }
    }
}

impl Eq for Device {}

impl Hash for Device {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        let (name, description) = match self {
            Device::Thermometer { name, description } => (name, description),
            Device::Socket {
                name, description, ..
            } => (name, description),
        };
        let mut values_to_hash = name.clone();
        values_to_hash.push_str(description);
        values_to_hash.hash(hasher)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read_sensor_reads_different_depending_on_socket_power() {
        let name = "test_name".to_string();
        let description = "test_description".to_string();
        let socket_on = Device::Socket {
            name: name.clone(),
            description: description.clone(),
            is_power_on: true,
        };
        let socket_off = Device::Socket {
            name: name.clone(),
            description: description.clone(),
            is_power_on: false,
        };

        let power_on_sensor_val = socket_on.read_sensor();
        let power_off_sensor_val = socket_off.read_sensor();

        assert_ne!(power_on_sensor_val, power_off_sensor_val);
    }
}
