use std::collections::HashSet;

use crate::errors::{DuplicateExistsError, NotFoundError};
use crate::rooms::Room;

pub struct Home {
    pub name: String,
    rooms: HashSet<Room>,
}

impl Home {
    pub fn new(name: String) -> Self {
        Self {
            name,
            rooms: HashSet::new(),
        }
    }

    pub fn add_room(&mut self, room: Room) -> Result<(), DuplicateExistsError> {
        if self.rooms.insert(room) {
            Ok(())
        } else {
            Err(DuplicateExistsError)
        }
    }

    pub fn find_room_by_name(&self, room_name: &str) -> Option<&Room> {
        self.rooms.get(&Room::new(room_name.to_string()))
    }

    pub fn remove_room_by_name(&mut self, room_name: &str) -> Result<(), NotFoundError> {
        if self.rooms.remove(&Room::new(room_name.to_string())) {
            Ok(())
        } else {
            Err(NotFoundError)
        }
    }

    pub fn get_display_str(&self) -> String {
        let mut buffer_str = format!("Home: {}\r\n------------\r\nDevices:\r\n\r\n", self.name,);
        for room in self.rooms.iter() {
            buffer_str.push_str(&format!("{}\r\n", room.get_display_str()));
        }
        buffer_str
    }
}

#[cfg(test)]
mod tests {
    extern crate spectral;
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn add_room_stores_new_room_returns_ok() {
        let mut home = Home::new("test_home".to_string());
        let room = Room::new("test_room".to_string());

        let result = home.add_room(room);

        assert_eq!(home.rooms.len(), 1);
        assert_that(&result).is_ok();
    }

    #[test]
    fn add_room_does_not_allow_duplicate_room_names_returns_duplicate_err() {
        let mut home = Home::new("test_home".to_string());
        let room_name = "test_room".to_string();
        let room = Room::new(room_name.clone());
        let duplicate_room = Room::new(room_name);
        home.add_room(room).ok();

        let result = home.add_room(duplicate_room);

        assert_eq!(home.rooms.len(), 1);
        assert_that(&result).is_err_containing(DuplicateExistsError);
    }

    #[test]
    fn find_room_by_name_retrieves_room_returns_some_value() {
        let mut home = Home::new("test_home".to_string());
        let room_name = "test_room".to_string();
        let room = Room::new(room_name.clone());
        home.add_room(room.clone()).ok();

        let option = home.find_room_by_name(&room_name);

        assert_eq!(option, Option::Some(&room));
    }

    #[test]
    fn find_room_by_name_if_not_found_returns_none() {
        let mut home = Home::new("test_home".to_string());
        let room = Room::new("test_room".to_string());
        home.add_room(room).ok();

        let option = home.find_room_by_name("different_name");

        assert_eq!(option, Option::None);
    }

    #[test]
    fn remove_room_by_name_removes_room_returns_ok() {
        let mut home = Home::new("test_home".to_string());
        let room_name = "test_room".to_string();
        let room = Room::new(room_name.clone());
        home.add_room(room).ok();

        let result = home.remove_room_by_name(&room_name);

        assert_eq!(home.rooms.len(), 0);
        assert_that(&result).is_ok();
    }

    #[test]
    fn remove_room_by_name_if_not_found_returns_not_found_err() {
        let mut home = Home::new("test_home".to_string());
        let room = Room::new("test_room".to_string());
        home.add_room(room).ok();

        let result = home.remove_room_by_name("different_name");

        assert_eq!(home.rooms.len(), 1);
        assert_that(&result).is_err_containing(NotFoundError);
    }
}
