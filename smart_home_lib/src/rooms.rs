use std::hash::{Hash, Hasher};

use crate::devices::*;
use crate::errors::*;

#[derive(Clone, Debug)]
pub struct Room {
    pub name: String,
    devices: Vec<Device>,
}

impl Room {
    pub fn new(name: String) -> Self {
        Self {
            name,
            devices: Vec::new(),
        }
    }

    pub fn add_device(&mut self, new_device: Device) -> Result<(), DuplicateExistsError> {
        if self.devices.iter().any(|device| *device == new_device) {
            Err(DuplicateExistsError)
        } else {
            self.devices.push(new_device);
            Ok(())
        }
    }

    pub fn find_device_by_name(&self, device_name: &str) -> Option<&Device> {
        self.devices.iter().find(|device| match device {
            Device::Thermometer { name, .. } => name == device_name,
            Device::Socket { name, .. } => name == device_name,
        })
    }

    pub fn remove_device_by_name(&mut self, device_name: &str) -> Result<(), NotFoundError> {
        if let Some(pos) = self.devices.iter().position(|device| match device {
            Device::Thermometer { name, .. } => name == device_name,
            Device::Socket { name, .. } => name == device_name,
        }) {
            self.devices.remove(pos);
            Ok(())
        } else {
            Err(NotFoundError)
        }
    }

    pub fn get_display_str(&self) -> String {
        let mut buffer_str = format!("Room: {}\r\nDevices:\r\n", self.name);
        for device in self.devices.iter() {
            buffer_str.push_str(&format!(" - {}\r\n", device.get_display_str()));
        }
        buffer_str
    }
}

impl PartialEq for Room {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Eq for Room {}

impl Hash for Room {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.name.hash(hasher);
    }
}

#[cfg(test)]
mod tests {
    extern crate spectral;
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn add_device_stores_new_device_returns_ok() {
        let mut room = Room::new("test_room".to_string());
        let device = Device::Thermometer {
            name: "test_name".to_string(),
            description: "test_description".to_string(),
        };

        let result = room.add_device(device);

        assert_eq!(room.devices.len(), 1);
        assert_that(&result).is_ok();
    }

    #[test]
    fn add_device_does_not_allow_duplicate_devices_returns_duplicate_err() {
        let mut room = Room::new("test_room".to_string());
        let device = Device::Thermometer {
            name: "test_name".to_string(),
            description: "test_description".to_string(),
        };
        let duplicate_device = Device::Thermometer {
            name: "other_name".to_string(),
            description: "other_description".to_string(),
        };
        room.add_device(device).ok();

        let result = room.add_device(duplicate_device);

        assert_eq!(room.devices.len(), 1);
        assert_that(&result).is_err_containing(DuplicateExistsError);
    }

    #[test]
    fn add_device_does_not_allow_duplicate_device_names_returns_duplicate_err() {
        let mut room = Room::new("test_room".to_string());
        let device_name = "duplicate_name".to_string();
        let device = Device::Thermometer {
            name: device_name.clone(),
            description: "test_description".to_string(),
        };
        let duplicate_device = Device::Socket {
            name: device_name,
            description: "other_description".to_string(),
            is_power_on: false,
        };
        room.add_device(device).ok();

        let result = room.add_device(duplicate_device);

        assert_eq!(room.devices.len(), 1);
        assert_that(&result).is_err_containing(DuplicateExistsError);
    }

    #[test]
    fn add_device_does_not_allow_duplicate_device_descriptions_returns_duplicate_err() {
        let mut room = Room::new("test_room".to_string());
        let device_description = "duplicate_description".to_string();
        let device = Device::Thermometer {
            name: "test_name".to_string(),
            description: device_description.clone(),
        };
        let duplicate_device = Device::Socket {
            name: "other_name".to_string(),
            description: device_description,
            is_power_on: true,
        };
        room.add_device(device).ok();

        let result = room.add_device(duplicate_device);

        assert_eq!(room.devices.len(), 1);
        assert_that(&result).is_err_containing(DuplicateExistsError);
    }

    #[test]
    fn find_device_by_name_retrieves_device_returns_some_value() {
        let mut room = Room::new("test_room".to_string());
        let device_name = "test_name".to_string();
        let device = Device::Thermometer {
            name: device_name.clone(),
            description: "test_description".to_string(),
        };
        room.add_device(device.clone()).ok();

        let option = room.find_device_by_name(&device_name);

        assert_eq!(option, Option::Some(&device));
    }

    #[test]
    fn find_device_by_name_if_not_found_returns_none() {
        let mut room = Room::new("test_room".to_string());
        let device = Device::Thermometer {
            name: "test_name".to_string(),
            description: "test_description".to_string(),
        };
        room.add_device(device).ok();

        let option = room.find_device_by_name("different_name");

        assert_eq!(option, Option::None);
    }

    #[test]
    fn remove_device_by_name_removes_device_returns_ok() {
        let mut room = Room::new("test_room".to_string());
        let device_name = "test_name".to_string();
        let device = Device::Thermometer {
            name: device_name.clone(),
            description: "test_description".to_string(),
        };
        room.add_device(device).ok();

        let result = room.remove_device_by_name(&device_name);

        assert_eq!(room.devices.len(), 0);
        assert_that(&result).is_ok();
    }

    #[test]
    fn remove_device_by_name_if_not_found_returns_not_found_err() {
        let mut room = Room::new("test_room".to_string());
        let device = Device::Thermometer {
            name: "test_name".to_string(),
            description: "test_description".to_string(),
        };
        room.add_device(device).ok();

        let result = room.remove_device_by_name("different_name");

        assert_eq!(room.devices.len(), 1);
        assert_that(&result).is_err_containing(NotFoundError);
    }
}
