#[derive(Debug, PartialEq)]
pub struct NotFoundError;

#[derive(Debug, PartialEq)]
pub struct DuplicateExistsError;
