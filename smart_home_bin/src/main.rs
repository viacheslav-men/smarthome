extern crate smart_home_lib;

use smart_home_lib::devices::*;
use smart_home_lib::home::*;
use smart_home_lib::rooms::*;

fn main() {
    let mut home = Home::new("Flat".to_string());
    println!("Setting up the home with devices...");

    let mut living_room = Room::new("Living room".to_string());
    living_room
        .add_device(Device::Socket {
            name: "TV socket".to_string(),
            description: "Turns on/off the TV".to_string(),
            is_power_on: false,
        })
        .ok();
    living_room
        .add_device(Device::Thermometer {
            name: "Room thermometer".to_string(),
            description: "Measures room temperature".to_string(),
        })
        .ok();

    if living_room
        .add_device(Device::Socket {
            name: "Radio socket".to_string(),
            description: "Turns on/off the radio".to_string(),
            is_power_on: false,
        })
        .is_err()
    {
        println!("Cannot add another device with the same type to the same room");
    }
    println!("Living room is set up!");

    let mut kitchen = Room::new("Kitchen".to_string());
    let common_device_name = "Washing machine socket".to_string();
    let common_device_description = "Turns on/off the washing machine".to_string();
    let kitchen_socket = Device::Socket {
        name: common_device_name.clone(),
        description: common_device_description.clone(),
        is_power_on: true,
    };
    kitchen.add_device(kitchen_socket).ok();

    if living_room
        .add_device(Device::Thermometer {
            name: common_device_name,
            description: "New description".to_string(),
        })
        .is_err()
    {
        println!("Cannot add another device with the same name to the same room");
    }

    if living_room
        .add_device(Device::Thermometer {
            name: "New name".to_string(),
            description: common_device_description,
        })
        .is_err()
    {
        println!("Cannot add another device with the same description to the same room");
    }
    println!("Kitchen is set up!");

    let bathroom_name = "Bathroom".to_string();
    let mut bathroom = Room::new(bathroom_name.clone());
    bathroom
        .add_device(Device::Thermometer {
            name: "Bath thermometer".to_string(),
            description: "Measures water temperature".to_string(),
        })
        .ok();
    let bathroom_socket_name = "Ventilation socket".to_string();
    bathroom
        .add_device(Device::Socket {
            name: bathroom_socket_name.clone(),
            description: "Turns on/off the ventilation".to_string(),
            is_power_on: true,
        })
        .ok();
    if bathroom
        .remove_device_by_name(&bathroom_socket_name)
        .is_ok()
    {
        println!("Broken device was de-installed!");
    }
    println!("Bathroom is set up!");

    home.add_room(kitchen).ok();
    home.add_room(living_room).ok();
    home.add_room(bathroom).ok();

    if home.add_room(Room::new(bathroom_name)).is_err() {
        println!("Cannot add another room with the same name");
    }
    println!("The whole home is set up!");

    println!("{}", home.get_display_str());
}
